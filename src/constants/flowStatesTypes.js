// @flow
import type {CommonActionType} from './flowActionTypes';

/* Messages */
export type MessagesState = {
    main: string,
    error: string,
}

/* Pagination */
export type PaginationState = {
    totalCount: number,
    totalRepos: number,
    itemsPerPage: number,
    offset: number,
    sortBy: string,
    order: string,
}

export type SingleRepositoryState = {
    id: number,
    repo_title: string,
    owner: string,
    stars: number,
    created_at: string
}

/* Storage checker */
export type StorageCheckerState = {
    checked: boolean,
    available: boolean
}

/* Common */
export type CommonState = {
    messages: MessagesState,
    pagination: PaginationState,
    repositoriesList: Array<SingleRepositoryState>,
    searchString: string,
    spinner: boolean,
    storageChecker: StorageCheckerState,
}
export type GetState = () => CommonState;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any; // eslint-disable-line no-use-before-define
export type PromiseAction = Promise<CommonActionType>;
export type Dispatch = (action: CommonActionType | ThunkAction | PromiseAction) => any;