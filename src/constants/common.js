// @flow
/* Items per page values */
export const TABLE_ROW_QTY_OPTIONS = [
    {value: 5, label: '5'},
    {value: 10, label: '10'},
    {value: 15, label: '15'},
    {value: 20, label: '20'},
];

/* Restrictions according to gitHub API */
export const MAX_REPOSITORIES_PER_REQUEST = 100;
export const MAX_REPOSITORIES_AVAILABLE_RESULT = 1000;

/* Repo fields as constants for Column Names and sorting */
export const REPO_ID_FIELD_NAME = 'id';
export const REPO_TITLE_FIELD_NAME = 'repo_title';
export const REPO_OWNER_FIELD_NAME = 'owner';
export const REPO_STARS_FIELD_NAME = 'stars';
export const REPO_CREATED_FIELD_NAME = 'created_at';

/* Sort orders */
export const REPO_SORT_ASC = 'asc';
export const REPO_SORT_DESC = 'desc';

/* Messages */
export const DEFAULT_MAIN_MESSAGE = `Start search gitHub repositories. You can get up ${MAX_REPOSITORIES_AVAILABLE_RESULT} repositories as result according to gitHub API.`;
export const DEFAULT_ERROR_MESSAGE = 'Oops... Something has gone wrong. You can send up 10 requests (1 request up to 100 repositories)' +
    ' per minute for unauthorized users and up to 30 requests for authorized users. Please, check your searchString or wait and try again.';
export const getSearchResultMessage = (result: number) => `Result: found ${result} ${result !== 1 ? 'repositories' : 'repository'}.`;