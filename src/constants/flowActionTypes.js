// @flow
import type {SingleRepositoryState} from './flowStatesTypes';

/* Messages */
export type MessagesActionTypes = { type: 'SET_INFORM_MESSAGES', params: Object }

/* Pagination */
export type PaginationActionTypes = { type: 'SET_PAGINATION_PARAMS', params: Object }

/* Repositories Action types */
export type RepositoriesListActionTypes = { type: 'SET_REPOSITORIES_LIST', repositoriesList: Array<SingleRepositoryState> }

/* Search string Action types */
export type SearchStringActionTypes = { type: 'SET_SEARCH_STRING', searchString: string }

/* Spinner Action types */
export type SpinnerActionTypes = { type: 'RUN_SPINNER' }
    | { type: 'STOP_SPINNER' }

/* Storage checker Action types */
export type StorageCheckerActionTypes = { type: 'SET_WEB_STORAGE_CHECKER', available: boolean }

export type CommonActionType = StorageCheckerActionTypes
    | SearchStringActionTypes
    | SpinnerActionTypes
    | PaginationActionTypes
    | RepositoriesListActionTypes
    | MessagesActionTypes
