// @flow

/* Messages */
export const SET_INFORM_MESSAGES = 'SET_INFORM_MESSAGES';

/* Pagination */
export const SET_PAGINATION_PARAMS = 'SET_PAGINATION_PARAMS';

/* Repositories */
export const SET_REPOSITORIES_LIST = 'SET_REPOSITORIES_LIST';


/* Search string */
export const SET_SEARCH_STRING = 'SET_SEARCH_STRING';

/* Spinner */
export const RUN_SPINNER = 'RUN_SPINNER';
export const STOP_SPINNER = 'STOP_SPINNER';

/* Storage */
export const SET_WEB_STORAGE_CHECKER = 'SET_WEB_STORAGE_CHECKER';
