// @flow

import {
    RUN_SPINNER, SET_INFORM_MESSAGES,
    SET_PAGINATION_PARAMS,
    SET_REPOSITORIES_LIST,
    SET_SEARCH_STRING,
    SET_WEB_STORAGE_CHECKER,
    STOP_SPINNER
} from '../constants/dispatchTypes';
import type {
    MessagesActionTypes,
    PaginationActionTypes, RepositoriesListActionTypes,
    SearchStringActionTypes, SpinnerActionTypes,
    StorageCheckerActionTypes
} from '../constants/flowActionTypes';
import type {SingleRepositoryState} from '../constants/flowStatesTypes';

export const runSpinner = (): SpinnerActionTypes => ({
    type: RUN_SPINNER,
});

export const stopSpinner = (): SpinnerActionTypes => ({
    type: STOP_SPINNER,
});


export const setSearchString = (searchString: string): SearchStringActionTypes => ({
    type: SET_SEARCH_STRING,
    searchString
});

export const setStorageChecker = (available: boolean): StorageCheckerActionTypes => ({
    type: SET_WEB_STORAGE_CHECKER,
    available
});

export const setPaginationParams = (params: Object): PaginationActionTypes => ({
    type: SET_PAGINATION_PARAMS,
    params
});

export const setRepositoriesList = (repositoriesList: Array<SingleRepositoryState>): RepositoriesListActionTypes => ({
    type: SET_REPOSITORIES_LIST,
    repositoriesList
});

export const setInfoMessages = (params: Object): MessagesActionTypes => ({
    type: SET_INFORM_MESSAGES,
    params
});