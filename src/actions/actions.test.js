import * as actions from './actions';

it('test deleteExtraRepoFields', () => {
    const repositoriesList = [
        {
            id: 1,
            name: 'test_name',
            created_at: '2016-02-01',
            stargazers_count: 200,
            owner: {login: 'pavel'},
            restField: 'unused_field'
        }
    ];
    const result = actions.deleteExtraRepoFields(repositoriesList);
    const {id, repo_title, owner, stars, created_at} = result[0];
    expect(id).toBe(1);
    expect(repo_title).toBe('test_name');
    expect(owner).toBe('pavel');
    expect(stars).toBe(200);
    expect(created_at).toBe('2016-02-01');
    expect(result.hasOwnProperty('restField')).toBeFalsy();
    expect(result.hasOwnProperty('owner')).toBeFalsy();
    expect(result.hasOwnProperty('stargazers_count')).toBeFalsy();
});