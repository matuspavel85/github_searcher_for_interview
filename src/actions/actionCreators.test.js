import * as actionCreators from './actionCreators';
import {RUN_SPINNER, SET_SEARCH_STRING} from '../constants/dispatchTypes';

it('test runSpinner', () => {
    const result = actionCreators.runSpinner();
    expect(result.type).toEqual(RUN_SPINNER);
});

it('test setSearchString', () => {
    const result = actionCreators.setSearchString('test_string');
    expect(result.type).toEqual(SET_SEARCH_STRING);
    expect(result.searchString).toEqual('test_string');
});