// @flow
import _orderby from 'lodash.orderby';

import {
    runSpinner,
    setInfoMessages,
    setPaginationParams,
    setRepositoriesList, setSearchString,
    setStorageChecker,
    stopSpinner
} from './actionCreators';
import {apiRequest, getSessionStorageValue, isStorageAvailable, setSessionStorageValue} from '../api/api';

import applicationStore from '../applicationStore';
import {
    getSearchResultMessage,
    DEFAULT_ERROR_MESSAGE,
    MAX_REPOSITORIES_AVAILABLE_RESULT,
    MAX_REPOSITORIES_PER_REQUEST,
    REPO_CREATED_FIELD_NAME,
    REPO_SORT_DESC,
} from '../constants/common';

import type {Dispatch, SingleRepositoryState} from '../constants/flowStatesTypes';

const REPOSITORY_LIST_STORAGE_KEY = 'repositories_list_';
const LAST_QUERYSTRING_STORAGE_KEY = 'last_query_string';

const generateStorageKey = (keyPrefix: string, queryString: string) => `${keyPrefix}${queryString}`;

export const deleteExtraRepoFields = (repositoriesList: Array<Object>): Array<SingleRepositoryState> => (
    repositoriesList.map(repository => {
        const {id, name: repo_title, created_at, stargazers_count: stars, owner: {login: owner}} = repository;
        return {id, repo_title, owner, stars, created_at};
    })
);

const checkIsStorageAvailable = (dispatch: Dispatch) => {
    const {storageChecker: {checked, available}} = applicationStore.getState();
    const canSaveToStorage = checked ? available : isStorageAvailable('sessionStorage');
    if (!checked) {
        dispatch(setStorageChecker(canSaveToStorage));
    }
    return canSaveToStorage;
};

export const sortRepositoriesList = (repositoriesList: Array<SingleRepositoryState>, sortBy: string, order: string) => (dispatch: Dispatch) => {
    if (sortBy === REPO_CREATED_FIELD_NAME) {
        const sortFunc = (a, b) => {
            const date1 = new Date(a.created_at);
            const date2 = new Date(b.created_at);
            return order === REPO_SORT_DESC ? date2 - date1 : date1 - date2;
        };
        dispatch(setRepositoriesList(repositoriesList.sort((a, b) => sortFunc(a, b))));
    } else {
        dispatch(setRepositoriesList(_orderby(repositoriesList, [sortBy], [order])));
    }
};

const loadLocalStoredData = (queryString: string) => (dispatch: Dispatch) => {
    const isStorageAvailable = checkIsStorageAvailable(dispatch);
    if (isStorageAvailable) {
        setSessionStorageValue(LAST_QUERYSTRING_STORAGE_KEY, queryString);
        const localStoredData = getSessionStorageValue(generateStorageKey(REPOSITORY_LIST_STORAGE_KEY, queryString));
        if (localStoredData) {
            const {pagination: {sortBy, order}} = applicationStore.getState();
            const {totalRepos, repositoriesList} = JSON.parse(localStoredData);
            dispatch(setPaginationParams({totalRepos, totalCount: repositoriesList.length, offset: 0}));
            dispatch(setInfoMessages({main: getSearchResultMessage(totalRepos)}));
            sortRepositoriesList(repositoriesList, sortBy, order)(dispatch);
            return true;
        }
        return false;
    }
    return false;
};

const processRepositoriesListResponse = (repositoriesList: Array<SingleRepositoryState>, queryString: string, totalRepos: number) => (dispatch: Dispatch) => {
    const canSaveToStorage = checkIsStorageAvailable(dispatch);
    if (canSaveToStorage) {
        setSessionStorageValue(generateStorageKey(REPOSITORY_LIST_STORAGE_KEY, queryString), JSON.stringify({
            totalRepos,
            repositoriesList
        }));
    }
    const {pagination: {sortBy, order}} = applicationStore.getState();
    sortRepositoriesList(repositoriesList, sortBy, order)(dispatch);
    dispatch(setPaginationParams({totalRepos, totalCount: repositoriesList.length, offset: 0}));
    dispatch(setInfoMessages({main: getSearchResultMessage(totalRepos)}));
};

const generateApiUrl = (queryString: string, page: number = 1) => (
    `https://api.github.com/search/repositories?q=${queryString}+in:name&page=${page}&sort=stars&per_page=${MAX_REPOSITORIES_PER_REQUEST}`
);

const apiLoadAllAvailableRepositories = (queryString: string, totalCount: number) => async (dispatch: Dispatch) => {
    const requestQty = Math.min(Math.ceil(totalCount / MAX_REPOSITORIES_PER_REQUEST), MAX_REPOSITORIES_AVAILABLE_RESULT / MAX_REPOSITORIES_PER_REQUEST);
    const requests = [];
    for (let page = 2; page <= requestQty; page++) {
        requests.push(apiRequest(generateApiUrl(queryString, page)));
    }
    const responsesList: Array<Object> = await Promise.all(requests);
    let hasError = false;
    const result = responsesList.reduce((result, repository) => {
        const {items, total_count, message} = repository;
        if (message && !hasError) {
            hasError = true;
        }
        return total_count ? [...result, ...deleteExtraRepoFields(items)] : result;
    }, []);
    if (hasError) {
        dispatch(setInfoMessages({error: DEFAULT_ERROR_MESSAGE}));
    }
    return result;
};

export const apiInitialLoadRepositoriesList = (queryString: string = 'react') => async (dispatch: Dispatch) => {
    if (!loadLocalStoredData(queryString)(dispatch)) {
        dispatch(runSpinner());
        const response = await apiRequest(generateApiUrl(queryString));
        if (response.items) {
            const {items, total_count} = response;
            if (total_count > MAX_REPOSITORIES_PER_REQUEST) {
                // flow-disable-next-line
                const restRepositories = await apiLoadAllAvailableRepositories(queryString, total_count)(dispatch);
                processRepositoriesListResponse([...deleteExtraRepoFields(items), ...restRepositories], queryString, total_count)(dispatch);
            } else {
                processRepositoriesListResponse(deleteExtraRepoFields(items), queryString, total_count)(dispatch);
            }
        } else {
            dispatch(setInfoMessages({error: DEFAULT_ERROR_MESSAGE}));
        }
        dispatch(stopSpinner());
    }
};

export const apiLoadLastRequest = () => (dispatch: Dispatch) => {
    if (checkIsStorageAvailable(dispatch)) {
        const lastQuery = getSessionStorageValue(LAST_QUERYSTRING_STORAGE_KEY);
        if (lastQuery) {
            dispatch(setSearchString(lastQuery));
            apiInitialLoadRepositoriesList(lastQuery)(dispatch);
        }
    }
};