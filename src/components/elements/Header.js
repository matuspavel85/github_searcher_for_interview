// @flow
import React from 'react';
import styles from '../../assets/styles/elements/header.module.scss';

const Header = () => <div className={styles.header}/>;

export default Header;