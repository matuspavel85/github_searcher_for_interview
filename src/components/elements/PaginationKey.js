// @flow
import * as React from 'react';
// flow-disable-next-line
import styles from '../../assets/styles/elements/pagination_key.module.scss';


type Props = {
    pageKey: React.Node | number,
    active?: boolean,
    onKeyClick: Function,
    newOffsetValue: number
}

const PaginationKey = (props: Props) => {
    const {pageKey, active, onKeyClick, newOffsetValue} = props;
    const handleClick = () => onKeyClick(newOffsetValue);
    return (
        <div className={active ? styles.active_key : styles.key}
             onClick={handleClick}
        >
            {pageKey}
        </div>
    );
};

export default PaginationKey;