import React from 'react';
import {shallow} from 'enzyme';
import PaginationKey from './PaginationKey';


test('test render PaginationKey', () => {
    const mockOnKeyClick = jest.fn();
    const props = {
        pageKey: 12,
        active: true,
        onKeyClick: mockOnKeyClick,
        newOffsetValue: 36
    };
    const wrapper = shallow(
        <PaginationKey {...props}/>
    );
    expect(wrapper).toMatchSnapshot();
    const key = wrapper.find('.active_key');
    expect(key).toBeDefined();
    expect(key.text()).toBe('12');
    key.simulate('click');
    expect(mockOnKeyClick).toHaveBeenCalled();
});
