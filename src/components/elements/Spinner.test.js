import React from 'react';
import {shallow} from 'enzyme';
import Spinner from './Spinner';

it('test render Spinner', () => {
    const wrapper = shallow(
        <Spinner/>
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.spinner')).toBeDefined();
});