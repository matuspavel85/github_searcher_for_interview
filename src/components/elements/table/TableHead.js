// @flow
import React from 'react';

import TableHeadColumn from './TableHeadColumn';

import styles from '../../../assets/styles/elements/table_content.module.scss';
import {
    REPO_CREATED_FIELD_NAME,
    REPO_ID_FIELD_NAME,
    REPO_OWNER_FIELD_NAME,
    REPO_STARS_FIELD_NAME,
    REPO_TITLE_FIELD_NAME
} from '../../../constants/common';

const TableHead = () => (
    <thead className={styles.thead}>
        <tr className={styles.row}>
            <TableHeadColumn field={REPO_ID_FIELD_NAME}/>
            <TableHeadColumn field={REPO_TITLE_FIELD_NAME}/>
            <TableHeadColumn field={REPO_OWNER_FIELD_NAME}/>
            <TableHeadColumn field={REPO_STARS_FIELD_NAME}/>
            <TableHeadColumn field={REPO_CREATED_FIELD_NAME}/>
        </tr>
    </thead>
);

export default TableHead;
