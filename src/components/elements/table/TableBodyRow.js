// @flow
import React from 'react';

import TableBodyColumn from './TableBodyColumn';

import styles from '../../../assets/styles/elements/table_content.module.scss';

import type {SingleRepositoryState} from '../../../constants/flowStatesTypes';

type Props = {
    repository: SingleRepositoryState
}

const TableBodyRow = ({repository}: Props) => {
    const {id, repo_title, owner, stars, created_at} = repository;
    return (
        <tr className={styles.row}>
            <TableBodyColumn text={id}/>
            <TableBodyColumn text={repo_title}/>
            <TableBodyColumn text={owner}/>
            <TableBodyColumn text={stars}/>
            <TableBodyColumn text={created_at.slice(0, 10)}/>
        </tr>
    );
};


export default TableBodyRow;
