// @flow
import React from 'react';

import styles from '../../../assets/styles/elements/table_content.module.scss';

type Props = {
    text: string | number
}

const TableBodyColumn = ({text}: Props) => (
    <td className={styles.column}>
        <div className={styles.text_cut}>
            {text}
        </div>
    </td>
);


export default TableBodyColumn;
