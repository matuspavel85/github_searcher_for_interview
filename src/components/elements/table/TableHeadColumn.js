// @flow
import React from 'react';
import {connect} from 'react-redux';
import _startcase from 'lodash.startcase';
import {bindActionCreators} from 'redux';

import {FaChevronDown, FaChevronUp} from 'react-icons/fa';

import {sortRepositoriesList} from '../../../actions/actions';
import {setPaginationParams} from '../../../actions/actionCreators';

import styles from '../../../assets/styles/elements/table_content.module.scss';

import {REPO_SORT_ASC, REPO_SORT_DESC} from '../../../constants/common';

import type {CommonState, Dispatch, PaginationState, SingleRepositoryState} from '../../../constants/flowStatesTypes';

type Props = {
    field: string,
    pagination: PaginationState,
    repositoriesList: Array<SingleRepositoryState>,
    actions: {
        sortRepositoriesList: Function,
        setPaginationParams: Function
    }
}

const TableHeadColumn = (props: Props) => {
    const {pagination: {sortBy, order}, field, repositoriesList, actions: {sortRepositoriesList, setPaginationParams}} = props;

    const getIconType = () => {
        if (sortBy === field) {
            return order === REPO_SORT_DESC ? FaChevronDown : FaChevronUp;
        }
        return FaChevronUp;
    };

    const getNewSortOrder = () => {
        if (sortBy === field) {
            return order === REPO_SORT_DESC ? REPO_SORT_ASC : REPO_SORT_DESC;
        }
        return REPO_SORT_DESC;
    };

    const onIconClick = (newOrder: string) => {
        sortRepositoriesList(repositoriesList, field, newOrder);
        setPaginationParams({sortBy: field, order: newOrder});
    };

    const getIcon = () => {
        const Component = getIconType();
        return <Component className={styles.icon}
                          onClick={() => onIconClick(getNewSortOrder())}
        />;
    };

    return (
        <th className={styles.column}>
            {_startcase(field)}
            {getIcon()}
        </th>
    );
};

const mapStateToProps = (state: CommonState) => ({
    pagination: state.pagination,
    repositoriesList: state.repositoriesList
});

const mapDispatchToProps = (dispatch: Dispatch) => (
    {
        actions: bindActionCreators({
            sortRepositoriesList,
            setPaginationParams
        }, dispatch)
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(TableHeadColumn);