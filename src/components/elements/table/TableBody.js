// @flow
import React from 'react';
import {connect} from 'react-redux';

import TableBodyRow from './TableBodyRow';

import styles from '../../../assets/styles/elements/table_content.module.scss';


import type {CommonState, PaginationState, SingleRepositoryState} from '../../../constants/flowStatesTypes';

type Props = {
    pagination: PaginationState,
    repositoriesList: Array<SingleRepositoryState>
}

const TableBody = (props: Props) => {
    const {pagination: {offset, itemsPerPage}, repositoriesList} = props;

    const getDisplayedReposList = () => repositoriesList.slice(offset, Math.min(repositoriesList.length, offset + itemsPerPage));

    const getRows = () => (
        getDisplayedReposList().map(repository => <TableBodyRow key={repository.id}
                                                                repository={repository}
        />)
    );
    return (
        <tbody className={styles.tbody}>
            {getRows()}
        </tbody>
    );
};

const mapStateToProps = (state: CommonState) => ({
    pagination: state.pagination,
    repositoriesList: state.repositoriesList,
});

export default connect(mapStateToProps)(TableBody);
