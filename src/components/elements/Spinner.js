// @flow
import React from 'react';
import styles from '../../assets/styles/elements/spinner.module.scss';


const Spinner = () => <div className={styles.spinner}/>;

export default Spinner;