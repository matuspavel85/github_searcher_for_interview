import React from 'react';
import {shallow} from 'enzyme';
import Dimmer from './Dimmer';


test('test render Dimmer', () => {
    const props = {
        children: <div className="test_child"/>
    };
    const wrapper = shallow(
        <Dimmer {...props}/>
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.dimmer')).toBeDefined();
    expect(wrapper.find('.content')).toBeDefined();
    expect(wrapper.find('.test_child')).toBeDefined();
});
