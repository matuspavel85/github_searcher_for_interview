// @flow
import * as React from 'react';
import styles from '../../assets/styles/elements/dimmer.module.scss';

const {dimmer: dimmerStyle, content: contentStyle} = styles;

type Props = {
    children: React.Node;
}

const Dimmer = (props: Props) => {
    const {children} = props;
    return (
        <div className={dimmerStyle}>
            <div className={contentStyle}>
                {children}
            </div>
        </div>
    );
};

export default Dimmer;