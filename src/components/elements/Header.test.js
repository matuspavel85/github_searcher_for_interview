import React from 'react';
import {shallow} from 'enzyme';
import Header from './Header';

it('test render Header', () => {
    const wrapper = shallow(
        <Header/>
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.spinner')).toBeDefined();
});