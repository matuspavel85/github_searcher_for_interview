// @flow
import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import ContainerHOC from './ContainerHOC';

import {apiInitialLoadRepositoriesList} from '../../actions/actions';
import {setInfoMessages, setSearchString} from '../../actions/actionCreators';

import styles from '../../assets/styles/containers/search.module.scss';

import type {CommonState, Dispatch, MessagesState} from '../../constants/flowStatesTypes';

type Props = {
    messages: MessagesState,
    searchString: string,
    actions: {
        apiInitialLoadRepositoriesList: Function,
        setSearchString: Function,
        setInfoMessages: Function,
    }
}

const SearchInputContainer = (props: Props) => {
    const {actions: {apiInitialLoadRepositoriesList, setSearchString, setInfoMessages}, searchString, messages: {error}} = props;

    const handleInputChange = ({target}) => {
        const {value} = target;
        setSearchString(value);
    };
    const handleButtonClick = () => {
        if (searchString.trim()) {
            if (error) {
                setInfoMessages({error: ''});
            }
            apiInitialLoadRepositoriesList(searchString);
        }
    };
    return (
        <Fragment>
            <label className={styles.label}>Search</label>
            <input value={searchString}
                   onChange={handleInputChange}
                   placeholder="Please, enter repository name"
                   className={styles.input}
            />
            <button onClick={handleButtonClick}
                    type="submit"
                    className={styles.button}
            >
                Search
            </button>
        </Fragment>
    );
};

const mapStateToProps = (state: CommonState) => ({
    messages: state.messages,
    searchString: state.searchString,
});

const mapDispatchToProps = (dispatch: Dispatch) => (
    {
        actions: bindActionCreators({
            apiInitialLoadRepositoriesList,
            setSearchString,
            setInfoMessages
        }, dispatch)
    }
);
export default connect(mapStateToProps, mapDispatchToProps)(ContainerHOC(SearchInputContainer)(styles.container));
