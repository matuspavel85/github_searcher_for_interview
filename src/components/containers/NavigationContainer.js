// @flow
import React, {Fragment} from 'react';

import ContainerHOC from './ContainerHOC';

import ItemsPageSelectContainer from './ItemsPageSelectContainer';
import SearchInputContainer from './SearchInputContainer';

import styles from '../../assets/styles/containers/navigation.module.scss';

const NavigationContainer = () => (
    <Fragment>
        <SearchInputContainer/>
        <ItemsPageSelectContainer/>
    </Fragment>
);

export default ContainerHOC(NavigationContainer)(styles.container);
