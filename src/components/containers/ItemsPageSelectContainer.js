// @flow
import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Select from 'react-select';

import {setPaginationParams} from '../../actions/actionCreators';

// flow-disable-next-line
import styles from '../../assets/styles/containers/select.module.scss';

import {TABLE_ROW_QTY_OPTIONS} from '../../constants/common';

import type {CommonState, Dispatch, PaginationState} from '../../constants/flowStatesTypes';

type Props = {
    pagination: PaginationState,
    actions: {
        setPaginationParams: Function
    }
}

const ItemsPageSelectContainer = (props: Props) => {
    const {pagination: {itemsPerPage}, actions: {setPaginationParams}} = props;

    const onChange = (selected: Object) => {
        const {value} = selected;
        setPaginationParams({itemsPerPage: value});
    };
    return (
        <Select value={TABLE_ROW_QTY_OPTIONS.find(option => option.value === itemsPerPage)}
                onChange={onChange}
                options={TABLE_ROW_QTY_OPTIONS}
                className={styles.container}
        />
    );
};

const mapStateToProps = (state: CommonState) => ({
    pagination: state.pagination
});

const mapDispatchToProps = (dispatch: Dispatch) => (
    {
        actions: bindActionCreators({
            setPaginationParams
        }, dispatch)
    }
);
export default connect(mapStateToProps, mapDispatchToProps)(ItemsPageSelectContainer);