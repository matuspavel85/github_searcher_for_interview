// @flow
import * as React from 'react';

const ContainerHOC = (Component: React.ComponentType<any>) => (className: string) => {
    const Wrapper = (props: Object) => (
        <div className={className}>
            <Component {...props}/>
        </div>
    );
    return Wrapper;
};
export default ContainerHOC;
