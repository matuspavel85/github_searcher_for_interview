// @flow
import React, {Fragment} from 'react';

import ContainerHOC from './ContainerHOC';
import NavigationContainer from './NavigationContainer';
import PaginationContainer from './PaginationContainer';
import MessagesContainer from './MessagesContainer';
import TableContainer from './TableContainer';

import styles from '../../assets/styles/containers/content.module.scss';

const ContentContainer = () => (
    <Fragment>
        <MessagesContainer/>
        <NavigationContainer/>
        <TableContainer/>
        <PaginationContainer/>
    </Fragment>
);

export default (ContainerHOC(ContentContainer)(styles.container));
