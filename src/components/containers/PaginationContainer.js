// @flow
import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import _range from 'lodash.range';

import {
    FaAngleDoubleLeft,
    FaAngleDoubleRight,
    FaAngleLeft,
    FaAngleRight,
} from 'react-icons/fa';

import ContainerHOC from './ContainerHOC';
import PaginationKey from '../elements/PaginationKey';

import {setPaginationParams} from '../../actions/actionCreators';

// flow-disable-next-line
import styles from '../../assets/styles/containers/pagination.module.scss';

import type {CommonState, Dispatch, PaginationState} from '../../constants/flowStatesTypes';

type Props = {
    pagination: PaginationState,
    actions: {
        setPaginationParams: Function
    }
}

const PaginationContainer = (props: Props) => {
    const PAGES_SHOW_LIMIT = 5;
    const {pagination: {totalCount, itemsPerPage, offset}, actions: {setPaginationParams}} = props;

    const pagesQty: number = totalCount % itemsPerPage === 0 ? totalCount / itemsPerPage : Math.floor(totalCount / itemsPerPage) + 1;
    const currentPage: number = offset ? offset / itemsPerPage + 1 : 1;
    const numbersAroundCenter: number = Math.floor(Math.min(PAGES_SHOW_LIMIT, pagesQty) / 2);
    const shouldShowAllArrows: boolean = pagesQty > PAGES_SHOW_LIMIT;
    const shouldShowToStart: boolean = shouldShowAllArrows && currentPage - numbersAroundCenter > 1;
    const shouldShowToEnd: boolean = shouldShowAllArrows && currentPage + numbersAroundCenter < pagesQty;

    const onKeyClick = (offset: number) => setPaginationParams({offset});

    const getPagesRange = () => {
        const maxPageNumber: number = Math.min(Math.max(currentPage + numbersAroundCenter, PAGES_SHOW_LIMIT), pagesQty);
        const minPageNumber: number = Math.max(maxPageNumber - numbersAroundCenter * 2, 1);
        return _range(minPageNumber, maxPageNumber + 1).map(page => (
            <PaginationKey key={page}
                           pageKey={page}
                           active={page === currentPage}
                           newOffsetValue={(page - 1) * itemsPerPage}
                           onKeyClick={onKeyClick}
            />)
        );
    };

    return (
        <Fragment>
            {shouldShowToStart &&
            <Fragment>
                <PaginationKey pageKey={<FaAngleDoubleLeft/>}
                               onKeyClick={onKeyClick}
                               newOffsetValue={0}
                />
                <PaginationKey pageKey={<FaAngleLeft/>}
                               onKeyClick={onKeyClick}
                               newOffsetValue={offset - itemsPerPage}
                />
            </Fragment>}
            {getPagesRange()}
            {shouldShowToEnd &&
            <Fragment>
                <PaginationKey pageKey={<FaAngleRight/>}
                               onKeyClick={onKeyClick}
                               newOffsetValue={currentPage * itemsPerPage}
                />
                <PaginationKey pageKey={<FaAngleDoubleRight/>}
                               onKeyClick={onKeyClick}
                               newOffsetValue={(pagesQty - 1) * itemsPerPage}
                />
            </Fragment>}
        </Fragment>
    );
};

const mapStateToProps = (state: CommonState) => ({
    pagination: state.pagination
});

const mapDispatchToProps = (dispatch: Dispatch) => (
    {
        actions: bindActionCreators({
            setPaginationParams
        }, dispatch)
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(ContainerHOC(PaginationContainer)(styles.container));
