// @flow
import React, {useEffect} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import ContainerHOC from './ContainerHOC';
import TableHead from '../elements/table/TableHead';

import TableBody from '../elements/table/TableBody';

import {apiLoadLastRequest} from '../../actions/actions';

import styles from '../../assets/styles/containers/table.module.scss';

import type {CommonState, Dispatch, SingleRepositoryState} from '../../constants/flowStatesTypes';

type Props = {
    repositoriesList: Array<SingleRepositoryState>,
    actions: {
        apiLoadLastRequest: Function
    }
}

const TableContainer = (props: Props) => {
    const {repositoriesList, actions: {apiLoadLastRequest}} = props;
    useEffect(() => {
        apiLoadLastRequest();
    }, []);
    return (
        repositoriesList.length > 0 &&
        <table className={styles.table}>
            <TableHead/>
            <TableBody/>
        </table>
    );
};

const mapStateToProps = (state: CommonState) => ({
    repositoriesList: state.repositoriesList,
});

const mapDispatchToProps = (dispatch: Dispatch) => (
    {
        actions: bindActionCreators({
            apiLoadLastRequest
        }, dispatch)
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(ContainerHOC(TableContainer)(styles.container));
