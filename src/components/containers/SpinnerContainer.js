// @flow
import React from 'react';

import {connect} from 'react-redux';

import Dimmer from '../elements/Dimmer';
import Spinner from '../elements/Spinner';

import type {CommonState} from '../../constants/flowStatesTypes';

type Props = {
    spinner: boolean;
}

const SpinnerContainer = (props: Props) => {
    const {spinner} = props;
    return (
        spinner &&
        <Dimmer>
            <Spinner/>
        </Dimmer>
    );
};

const mapStateToProps = (state: CommonState) => ({
    spinner: state.spinner
});

export default connect(mapStateToProps)(SpinnerContainer);