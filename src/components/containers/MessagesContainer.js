// @flow
import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import ContainerHOC from './ContainerHOC';

import styles from '../../assets/styles/containers/messages.module.scss';

import type {CommonState, MessagesState} from '../../constants/flowStatesTypes';

type Props = {
    messages: MessagesState,
}

const MessagesContainer = (props: Props) => {
    const {messages: {main, error}} = props;

    return (
        <Fragment>
            <div className={styles.main}>{main}</div>
            <div className={styles.error}>{error}</div>
        </Fragment>
    );
};

const mapStateToProps = (state: CommonState) => ({
    messages: state.messages,
});
export default connect(mapStateToProps)(ContainerHOC(MessagesContainer)(styles.container));
