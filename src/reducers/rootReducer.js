import {combineReducers} from 'redux';

import messages from './messagesReducer';
import pagination from './paginationReducer';
import repositoriesList from './repositoriesListReducer';
import searchString from './searchStringReducer';
import spinner from './spinnerReducer';
import storageChecker from './storageCheckerReducer';

export default combineReducers({
    messages,
    pagination,
    repositoriesList,
    searchString,
    spinner,
    storageChecker,
});