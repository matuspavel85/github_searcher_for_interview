// @flow
import {SET_WEB_STORAGE_CHECKER} from '../constants/dispatchTypes';
import type {StorageCheckerState} from '../constants/flowStatesTypes';
import type {StorageCheckerActionTypes} from '../constants/flowActionTypes';

const initialState = {
    checked: false,
    available: false
};

function storageCheckerReducer(state: StorageCheckerState = initialState, action: StorageCheckerActionTypes): StorageCheckerState {
    const {type, available} = action;
    if (type === SET_WEB_STORAGE_CHECKER) {
        return {
            checked: true,
            available
        };
    }
    return state;
}

export default storageCheckerReducer;