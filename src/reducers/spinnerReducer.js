// @flow

import {RUN_SPINNER, STOP_SPINNER} from '../constants/dispatchTypes';
import type {SpinnerActionTypes} from '../constants/flowActionTypes';

function spinner(state: boolean = false, action: SpinnerActionTypes): boolean {
    switch (action.type) {
        case RUN_SPINNER:
            return true;
        case STOP_SPINNER:
            return false;
        default:
            return state;
    }
}

export default spinner;