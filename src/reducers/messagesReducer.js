// @flow

import {SET_INFORM_MESSAGES} from '../constants/dispatchTypes';
import {DEFAULT_MAIN_MESSAGE} from '../constants/common';
import type {MessagesState} from '../constants/flowStatesTypes';
import type {MessagesActionTypes} from '../constants/flowActionTypes';

const initialState = {
    main: DEFAULT_MAIN_MESSAGE,
    error: '',
};

function messagesReducer(state: MessagesState = initialState, action: MessagesActionTypes): MessagesState {
    const {type, params} = action;
    if (type === SET_INFORM_MESSAGES) {
        return {
            ...state,
            ...params
        };
    }
    return state;
}

export default messagesReducer;