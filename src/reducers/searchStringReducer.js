// @flow
import {SET_SEARCH_STRING} from '../constants/dispatchTypes';
import type {SearchStringActionTypes} from '../constants/flowActionTypes';

function searchStringReducer(state: string = '', action: SearchStringActionTypes): string {
    const {type, searchString} = action;
    if (type === SET_SEARCH_STRING) {
        return searchString;
    }
    return state;
}

export default searchStringReducer;
