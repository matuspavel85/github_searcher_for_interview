// @flow

import {SET_REPOSITORIES_LIST} from '../constants/dispatchTypes';
import type {SingleRepositoryState} from '../constants/flowStatesTypes';
import type {RepositoriesListActionTypes} from '../constants/flowActionTypes';

function repositoriesListReducer(state: Array<SingleRepositoryState> = [], action: RepositoriesListActionTypes): Array<SingleRepositoryState> {
    const {type, repositoriesList} = action;
    if (type === SET_REPOSITORIES_LIST) {
        return repositoriesList;
    }
    return state;
}

export default repositoriesListReducer;