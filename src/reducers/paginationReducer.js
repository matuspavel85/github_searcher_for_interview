// @flow

import {SET_PAGINATION_PARAMS} from '../constants/dispatchTypes';
import {REPO_SORT_DESC, REPO_STARS_FIELD_NAME} from '../constants/common';
import type {PaginationState} from '../constants/flowStatesTypes';
import type {PaginationActionTypes} from '../constants/flowActionTypes';

const initialState = {
    totalRepos: 0,
    totalCount: 0,
    itemsPerPage: 15,
    offset: 0,
    sortBy: REPO_STARS_FIELD_NAME,
    order: REPO_SORT_DESC
};

function paginationReducer(state: PaginationState = initialState, action: PaginationActionTypes): PaginationState {
    const {type, params} = action;
    if (type === SET_PAGINATION_PARAMS) {
        return {
            ...state,
            ...params
        };
    }
    return state;
}

export default paginationReducer;