// @flow

export const isStorageAvailable = (storageType: string) => {
    try {
        const storage = window[storageType];
        const testData = 'test_storage';
        storage.setItem(testData, testData);
        storage.removeItem(testData);
        return true;
    } catch (e) {
        return false;
    }
};

export const setSessionStorageValue = (key: string, value: string) => sessionStorage.setItem(key, value);

export const getSessionStorageValue = (key: string): string => sessionStorage.getItem(key) || '';

const makeRequest = (url: string) => (
    fetch(url)
        .then(response => {
            if (response.status === 200 || response.status === 400) {
                return response.json();
            }
            return Promise.reject();
        }).catch(() => ({message: 'serverError'}))
);

export async function apiRequest(url: string): Object {
    const response = await makeRequest(url);
    return response;
}
