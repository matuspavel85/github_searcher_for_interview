// @flow
import React from 'react';

import ContentContainer from './components/containers/ContentContainer';
import SpinnerContainer from './components/containers/SpinnerContainer';
import Header from './components/elements/Header';

/* Test gitlab rebase */
/* Test gitlab rebase 2 */
/* Test gitlab rebase 5 */
const App = () => (
    <div>
        <Header/>
        <SpinnerContainer/>
        <ContentContainer/>
    </div>
);

export default App;

/* test rebase*/
