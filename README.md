
## Available Scripts

According to notifications in the task description, I've used create-react-app with its default configuration.
 
You can run default create-react-app scripts and few custom:

#### `npm start`
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### `npm test`
Runs tests

#### `npm run build`
Builds the app for production to the `build` folder.<br>

#### `npm run lint`
Runs eslint code checking

#### `npm run flow`
Manually runs flow-typed code checking


## Realised tasks from checklist:

* #### `Render table with repository ID, title, owner, starts and created at timestamp`

* #### `Render an <input> element to search for repositories by name`

**Note:**
There is not any information how many repositories we should load from gitHub so my app returns as many as possible: per request - 100, max for one query string - 1000(sorted by stars quantity). API request is realised as simple as it is necessary to current task.

* #### `Cache the results of every search - do not make an API request if the results are already stored, instead show them from some storage like localStorage`

**Note:**
I've used `sessionStorage` instead `localStorage`. Reason: repositories total_count and repository stars are dynamical, so there is no reason to save it more then session duration. If we need to switch to `localStorage` - should change only one variable.

* #### `Add ability to control number of rows rendered per page (5 - default/10/15/20)`
* #### `Implement ASC/DESC sorting by every column`
* #### `Persist last search query and results - when we go back to the page or refresh it should show the last results of search`

#### Not realised:
* #### `Add GitHub authentication mechanism (eg: Log in with GitHub button) and highlight the row of repository of the logged-in user - please note that some users have 2FA enabled`


## Realised tasks from bonus list:
* #### `Use linting and/or formatting tool - ESLint, Prettier (if you use @callstack/eslint-config you should be good to go)`
* #### `Use type checker - Flow or Typescript`
* #### `Write test suites - for example with Jest`
**Note:**
There are only test examples, I hope there is no reason in full test coverage for test project.
* #### `Write good README in terms of instructions for reviewers`
**Note:**
If you are still reading  this manual, my README is not so bad:)

